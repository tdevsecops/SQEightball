# Most jobs (unless indicated otherwise) in this workflow require the following 
# GitLab CI/CD variables to be configured, either at group level 
# (Group Settings -> CI/CD -> Variables) or at Repository level 
# (Repository Settings -> CI/CD -> Variables)
# - FOD_TENANT
# - FOD_USERNAME
# - FOD_PAT
# - FOD_RELEASE
# Optionally you can override the following variables when using the templates
# - FOD_URL
#   Default value: https://ams.fortify.com
# - FOD_API_URL
#   Default value: https://api.ams.fortify.com/
# - FOD_UPLOADER_OPTS
#   Default value: -ep 2 -pp 0
# - FOD_NOTES
#   Default value: Triggered by Gitlab Pipeline IID $CI_PIPELINE_IID: $CI_PIPELINE_URL
# - PACKAGE_OPTS
#   Default value: -bt mvn

# Remotely include one of the official Fortify templates. For this example we include multiple
# templates to demonstrate each of them.
include: 
  # Use this template to initiate an FoD SAST scan
  - project: 'Fortify/gitlab-ci-templates'
    file: 'templates/fortify-sast-fod.yml'
  # Use this template to initiate an FoD SAST scan, wait for scan completion, 
  # and report SAST vulnerabilities back to GitLab
  - project: 'Fortify/gitlab-ci-templates'
    file: 'templates/fortify-sast-fod-with-report.yml'

# Example demonstrating how to customize the template job defined in 
# fortify-sast-fod.yml, overriding default variables in the template
fortify-sast-fod:
  variables:
    # In addition to starting a scan, also wait for the scan to complete and display summarized results ('-I 1') and do not fail the job if the release is failing the Security Policy set in Fortify on Demand ('-apf' combined with allow_failure: true) 
    FOD_UPLOADER_OPTS: "-ep 2 -pp 0 -I 1 -apf"
  # Specify the events that should trigger the Fortify on Demand job for this repository. For this demo, we want to manually run. A common configuration for a SAST scan would be to trigger on one or more of merge/schedule/release trigger against master
  when: manual
  allow_failure: true

# Example demonstrating how to customize the template job defined in 
# fortify-sast-fod-with-report.yml, overriding default variables in the template
fortify-sast-fod-with-report:
  variables:
    # In addition to standard options, do not fail the job if the release is failing the Security Policy set in Fortify on Demand ('-apf' combined with allow_failure: true) 
    FOD_UPLOADER_OPTS: "-ep 2 -pp 0 -apf"
  # Specify the events that should trigger the Fortify on Demand job for this repository. For this demo, we want to manually run. A common configuration for a SAST scan would be to trigger on one or more of merge/schedule/release trigger against master
  when: manual
  allow_failure: true

# Example demonstrating how to use FortifyToolsInstaller in combination with
# standard Fortify commands to run an FoD scan and report results back to GitLab,
# useful in the following situations:
#  - You prefer full visibility of the various steps
#  - The fortify-ci-tools image used by the templates is not suitable for 
#    building your application
#  - The templates do not fit your needs for any other reason
fortify-sast-fod-using-tools-installer:
  image: maven:3.8.4-jdk-11
  # Specify the events that should trigger the Fortify on Demand job for this repository. For this demo, we want to manually run. A common configuration for a SAST scan would be to trigger on one or more of merge/schedule/release trigger against master
  when: manual
  script:
      # Install fortify tools
      # See https://github.com/fortify/FortifyToolsInstaller#readme
    - FTI_TOOLS=ScanCentral,FoDUpload,FortifyVulnerabilityExporter source <(curl -sL https://raw.githubusercontent.com/fortify/FortifyToolsInstaller/v2/FortifyToolsInstaller.sh)
      # Package source code using Maven build tool integration
      # See https://www.microfocus.com/documentation/fortify-software-security-center/2110/SC_SAST_Help_21.1.0/index.htm#Gen_SC_Package.htm
    - scancentral package -bt mvn -o package.zip
      # Submit package to be scanned to FoD, and wait for scan completion
      # See https://github.com/fod-dev/fod-uploader-java#readme
    - FoDUpload -z package.zip -aurl https://api.ams.fortify.com -purl https://ams.fortify.com -rid "$FOD_RELEASE" -tc "$FOD_TENANT" -uc "$FOD_USERNAME" "$FOD_PAT" -ep 2 -pp 0 -I 1 -apf -n "Submitted from GitLab Pipeline"
      # Generate a GitLab SAST report
      # See https://github.com/fortify/FortifyVulnerabilityExporter#readme
    - FortifyVulnerabilityExporter FoDToGitLabSAST --fod.baseUrl=https://ams.fortify.com --fod.tenant=$FOD_TENANT --fod.userName=$FOD_USERNAME --fod.password=$FOD_PAT --fod.release.id=$FOD_RELEASE
  artifacts:
    reports:
      sast: gl-fortify-sast.json


# This workflow requires the following GitLab variables to be configured:
#  FCLI_SSC_URL
#  FCLI_SSC_USER
#  FCLI_SSC_PASSWORD
#  FCLI_SC_SAST_CLIENT_AUTH_TOKEN
#  FCLI_SC_SAST_SCAN_CI_TOKEN
fortify-dast-and-sast-with-fcli:
  image: rsenden/fortify-ci-tools:fcli-beta
  when: manual
  script:
    - fcli ssc session login
    - fcli sc-sast session login
    - fcli sc-dast session login

    # Create appversion if not yet existing
    - fcli ssc appversion create FcliTestApp:v1 --issue-template "Prioritized High Risk Issue Template" --auto-required-attrs --skip-if-exists --store myVersion:id

    # Package sources & run SAST scan
    - scancentral package -bt mvn -o package.zip 
    - fcli sc-sast scan start package package.zip --sensor-version 22.1 --appversion {?myVersion:id} --store '?'

    # Run DAST scan. Note that we don't have commands yet for configuring a scan for a particular
    # application version, so results will be published to the application version associated with
    # the given CICDToken
    - fcli sc-dast scan start MyScan -S 011daf6b-f2d0-4127-89ab-1d3cebab8784 --store '?'

    # Wait for both SAST & DAST scan to complete
    - fcli sc-sast scan wait-for-artifact '?' -i 30s
    - fcli sc-dast scan wait-for '?' -i 30s

    # Run FortifyVulnerabilityExporter; usually you'd have SAST & DAST results uploaded to single app version,
    # so you'd need to invoke the exporter only once.
    - FortifyVulnerabilityExporter SSCToGitLabSAST --ssc.baseUrl=$FCLI_SSC_URL --ssc.user="$FCLI_SSC_USER" --ssc.password="$FCLI_SSC_PASSWORD" --ssc.version.id=$(fcli config var contents get myVersion -o expr={id})
    - FortifyVulnerabilityExporter SSCToGitLabDAST --ssc.baseUrl=$FCLI_SSC_URL --ssc.user="$FCLI_SSC_USER" --ssc.password="$FCLI_SSC_PASSWORD" --ssc.version.id=10009

    # For now, just delete FcliTestApp so we can rerun the same workflow without any potential artifact approval required issues
    - fcli ssc app delete FcliTestApp --delete-versions

    # Clean up tokens, session variables, ...
    - fcli sc-dast session logout
    - fcli sc-sast session logout
    - fcli ssc session logout
  artifacts:
    reports:
      sast: gl-fortify-sast.json
      dast: gl-fortify-dast.json
      
# This workflow requires the following GitLab variables to be configured:
#  FCLI_DEFAULT_SSC_URL
#  FCLI_DEFAULT_SSC_USER
#  FCLI_DEFAULT_SSC_PASSWORD
#  FCLI_DEFAULT_SC_SAST_CLIENT_AUTH_TOKEN
#  FCLI_DEFAULT_SC_SAST_SCAN_CI_TOKEN
fortify-dast-and-sast-with-fcli-win:
  tags:
    - shared-windows
    - windows
  image: rsenden/fortify-ci-tools:fcli-beta-win
  when: manual
  script:
    - fcli ssc session login
    - fcli sc-sast session login
    - fcli sc-dast session login

    # Create appversion if not yet existing
    - fcli ssc appversion create FcliTestApp:v1 --issue-template "Prioritized High Risk Issue Template" --auto-required-attrs --skip-if-exists --store myVersion:id

    # Package sources & run SAST scan
    - scancentral package -bt mvn -o package.zip 
    - fcli sc-sast scan start package package.zip --sensor-version 22.1 --appversion {?myVersion:id} --store '?'

    # Run DAST scan. Note that we don't have commands yet for configuring a scan for a particular
    # application version, so results will be published to the application version associated with
    # the given CICDToken
    - fcli sc-dast scan start MyScan -S 011daf6b-f2d0-4127-89ab-1d3cebab8784 --store '?'

    # Wait for both SAST & DAST scan to complete
    - fcli sc-sast scan wait-for-artifact '?' -i 30s
    - fcli sc-dast scan wait-for '?' -i 30s

    # Run FortifyVulnerabilityExporter; usually you'd have SAST & DAST results uploaded to single app version,
    # so you'd need to invoke the exporter only once.
    - FortifyVulnerabilityExporter SSCToGitLabSAST --ssc.baseUrl=$FCLI_SSC_URL --ssc.user="$FCLI_SSC_USER" --ssc.password="$FCLI_SSC_PASSWORD" --ssc.version.id=$(fcli config var contents get myVersion -o expr={id})
    - FortifyVulnerabilityExporter SSCToGitLabDAST --ssc.baseUrl=$FCLI_SSC_URL --ssc.user="$FCLI_SSC_USER" --ssc.password="$FCLI_SSC_PASSWORD" --ssc.version.id=10009

    # For now, just delete FcliTestApp so we can rerun the same workflow without any potential artifact approval required issues
    - fcli ssc app delete FcliTestApp --delete-versions

    # Clean up tokens, session variables, ...
    - fcli sc-dast session logout
    - fcli sc-sast session logout
    - fcli ssc session logout
  artifacts:
    reports:
      sast: gl-fortify-sast.json
      dast: gl-fortify-dast.json

fortify-dast-and-sast-with-fcli-win-no-image:
  tags:
    - shared-windows
    - windows
  when: manual
  script:
    # Install fcli & other fortify tools
    - cmd.exe /c curl -fSLo fcli-windows.zip https://github.com/fortify-ps/fcli/releases/download/dev_main/fcli-windows.zip
    - Expand-Archive -Path fcli-windows.zip -DestinationPath C:\fcli\bin
    - del fcli-windows.zip
    - setx /M "$Env:PATH;c:\fcli\bin;c:\fu\bin;c:\sc\bin;c:\fve\bin"
    - refreshenv
    - fcli tool fod-uploader install 5.4.0 -d c:\fu
    - fcli tool sc-client install 22.1.2 -d c:\sc
    - fcli tool vuln-exporter install 1.8.0 -d c:\fve
    
    - fcli ssc session login
    - fcli sc-sast session login
    - fcli sc-dast session login

    # Create appversion if not yet existing
    - fcli ssc appversion create FcliTestApp:v1 --issue-template "Prioritized High Risk Issue Template" --auto-required-attrs --skip-if-exists --store myVersion:id

    # Package sources & run SAST scan
    - scancentral package -bt mvn -o package.zip 
    - fcli sc-sast scan start package package.zip --sensor-version 22.1 --appversion {?myVersion:id} --store '?'

    # Run DAST scan. Note that we don't have commands yet for configuring a scan for a particular
    # application version, so results will be published to the application version associated with
    # the given CICDToken
    - fcli sc-dast scan start MyScan -S 011daf6b-f2d0-4127-89ab-1d3cebab8784 --store '?'

    # Wait for both SAST & DAST scan to complete
    - fcli sc-sast scan wait-for-artifact '?' -i 30s
    - fcli sc-dast scan wait-for '?' -i 30s

    # Run FortifyVulnerabilityExporter; usually you'd have SAST & DAST results uploaded to single app version,
    # so you'd need to invoke the exporter only once.
    - FortifyVulnerabilityExporter SSCToGitLabSAST --ssc.baseUrl=$FCLI_SSC_URL --ssc.user="$FCLI_SSC_USER" --ssc.password="$FCLI_SSC_PASSWORD" --ssc.version.id=$(fcli config var contents get myVersion -o expr={id})
    - FortifyVulnerabilityExporter SSCToGitLabDAST --ssc.baseUrl=$FCLI_SSC_URL --ssc.user="$FCLI_SSC_USER" --ssc.password="$FCLI_SSC_PASSWORD" --ssc.version.id=10009

    # For now, just delete FcliTestApp so we can rerun the same workflow without any potential artifact approval required issues
    - fcli ssc app delete FcliTestApp --delete-versions

    # Clean up tokens, session variables, ...
    - fcli sc-dast session logout
    - fcli sc-sast session logout
    - fcli ssc session logout
  artifacts:
    reports:
      sast: gl-fortify-sast.json
      dast: gl-fortify-dast.json